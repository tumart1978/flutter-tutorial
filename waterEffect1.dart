import 'dart:math';

import 'package:flutter/material.dart';
import 'dart:math' as Math;

class WaterEffect1 extends StatefulWidget {
  WaterEffect1({Key key}) : super(key: key);

  @override
  _WaterEffect1State createState() => _WaterEffect1State();
}

class _WaterEffect1State extends State<WaterEffect1>
    with SingleTickerProviderStateMixin {
  ValueNotifier<double> sliderData = new ValueNotifier<double>(0.2);
  WaterEffectPainter _waterEffectPainter = new WaterEffectPainter();

  AnimationController _animationController;
  Animation _animation;

  // lets make tween for animation ticker

  @override
  void initState() {
    super.initState();
    // lets Start!

    // this duration u can set anything..
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 5));

    _animation = Tween<double>(begin: 0.0, end: 1.0).animate(
        _animationController)
      ..addListener(() =>
          _waterEffectPainter.setAnimationValue(_animationController.value));

    _waterEffectPainter.setSliderHeight(sliderData.value);
    _waterEffectPainter.setAnimationValue(_animationController.value);
    // my mistake.. need run animator controller 1st. haha
    _animationController.repeat(reverse: true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.red,
        child: SafeArea(
          child: ValueListenableBuilder(
            valueListenable: sliderData,
            builder: (context, double value, Widget child) {
              return Stack(
                children: [
                  LayoutBuilder(
                    builder: (context, constraints) {
                      return Container(
                        alignment: Alignment.bottomCenter,
                        child: CustomPaint(
                          // lets make custom painter
                          // ops..let make responsive painter
                          foregroundPainter: _waterEffectPainter,
                          child: Container(
                            // color: Colors.yellow,
                            height: constraints.maxHeight * value / 1,
                          ),
                        ),
                      );
                    },
                  ),
                  Positioned(
                    bottom: 20,
                    child: Slider(
                      // not move because it listen but no widget to refresh
                      onChanged: (value) {
                        sliderData.value = value;
                        sliderData.notifyListeners();
                      },
                      value: value,
                      min: 0.2,
                      max: 1,
                    ),
                  )
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}

class WaterEffectPainter extends CustomPainter with ChangeNotifier {
  double animationValue = 0;
  double sliderValue = 5;
  double degree2Radian;

  WaterEffectPainter() {
    this.degree2Radian = Math.pi / 180.0;
  }

  @override
  void paint(Canvas canvas, Size size) {
    List<Map<String, dynamic>> waves = new List<Map<String, dynamic>>();

    // increment ,front on top
    waves.addAll([
      {
        'speed': 7.0,
        'paint': Paint()
          ..color = Colors.blue
          ..style = PaintingStyle.fill,
        'stroke': Paint()
          ..color = Colors.white
          ..style = PaintingStyle.stroke
          ..strokeWidth = 3,
        'offsetX': 0.0, // make ocean offset x axis later,
        'bend': 7.0 // make each ocean bend different
      },
      {
        'speed': 5.0,
        'paint': Paint()
          ..color = Colors.blue[700]
          ..style = PaintingStyle.fill,
        'stroke': Paint()
          ..color = Colors.white
          ..style = PaintingStyle.stroke
          ..strokeWidth = 3,
        'offsetX': 3.0, // make ocean offset x axis later,
        'bend': 10.0, // make each ocean bend different
        'reverse': true,
      },
      {
        'speed': 3.0,
        'paint': Paint()
          ..color = Colors.blue[900]
          ..style = PaintingStyle.fill,
        'stroke': Paint()
          ..color = Colors.white
          ..style = PaintingStyle.stroke
          ..strokeWidth = 3,
        'offsetX': 3.0, // make ocean offset x axis later,
        'bend': 10.0 // make each ocean bend different
      }
    ]);

    buildWaves(canvas, size, waves: waves, offsetY: 30.0);

    // yeay.. lastly, make reverse for ocean
  }

  void buildWaves(Canvas canvas, Size size,
      {List<Map<String, dynamic>> waves, offsetY = 20.0}) {
    double offsetYValue = -(offsetY * waves.length);

    waves.reversed.toList().asMap().entries.forEach((row) {
      Path pathWave = new Path();
      Paint paint = row.value['paint'];
      Paint paintStroke = row.value['stroke'];
      double bend = row.value['bend'] ?? 7.0;
      double speed = row.value['speed'] ?? 5.0;
      bool reverse = row.value['reverse'] ?? false;

      offsetYValue += row.key != 0 ? offsetY : 0;

      // make stroke

      // how to make multiple ocean?
      // lets duplicate

      // let generate function generate wave sin so we can use for later
      pathWave.extendWithPath(
          generateSinWave(size,
              totalPoint: 3,
              bendRadius: bend,
              speedWave: speed,
              reverse: reverse),
          Offset(0, offsetYValue)); // make ocean shadow more top
      canvas.drawPath(pathWave, paintStroke);

      pathWave.lineTo(size.width, size.height);
      pathWave.lineTo(0, size.height);

      canvas.drawPath(pathWave, paint);
    });

    // can see stroke on right ocean..not good..
  }

  Path generateSinWave(size,
      {double bendRadius,
      double totalPoint,
      double speedWave: 1,
      bool reverse}) {
    // generate list sin wave offset point
    List<Offset> sinWaves = new List<Offset>();
    for (int i = 0; i <= size.width; i++) {
      sinWaves.add(new Offset(
          i.toDouble(),
          sin((speedWave *
                          (reverse
                              ? 1 - this.animationValue
                              : this.animationValue) *
                          360 -
                      i * totalPoint) %
                  360 *
                  degree2Radian) *
              bendRadius));
    }

    Path path = new Path();
    path.addPolygon(sinWaves.toList(), false);
    return path;
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // later can customize
    return true;
  }

  setAnimationValue(double value) {
    this.animationValue = value;
    this.notifyListeners();
    // we use change notifier in painter
  }

  void setSliderHeight(double value) {
    this.sliderValue = value;
    this.notifyListeners();
  }
}
